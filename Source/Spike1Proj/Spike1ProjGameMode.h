// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "Spike1ProjGameMode.generated.h"

UCLASS(minimalapi)
class ASpike1ProjGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	ASpike1ProjGameMode();
};



