// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#include "Spike1ProjGameMode.h"
#include "Spike1ProjHUD.h"
#include "Spike1ProjCharacter.h"
#include "UObject/ConstructorHelpers.h"

ASpike1ProjGameMode::ASpike1ProjGameMode()
	: Super()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnClassFinder(TEXT("/Game/FirstPersonCPP/Blueprints/FirstPersonCharacter"));
	DefaultPawnClass = PlayerPawnClassFinder.Class;

	// use our custom HUD class
	HUDClass = ASpike1ProjHUD::StaticClass();
}
