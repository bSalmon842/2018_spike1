---
Spike Report
---

Core Spike 1
============

Introduction
------------

This spike teaches the user how to setup an Unreal Engine 4 Project with
Source Control via Bitbucket and SourceTree. This skill is vital for any
game development task due to the potential damage of losing data or
being unable to access previous versions.

Bitbucket Link: <https://bitbucket.org/bSalmon842/2018_spike1>

Goals
-----

1.  A git repository, which contains:

    a.  a proper gitignore file for Unreal Engine as the first commit

    b.  the Unreal Engine First Person Shooter C++ project, without
        Starter Materials

Personnel
---------

  ------------------------- ------------------
  Primary -- Brock Salmon   Secondary -- N/A
  ------------------------- ------------------

Technologies, Tools, and Resources used
---------------------------------------

-   <https://bitbucket.org/>

-   <https://www.sourcetreeapp.com/>

Tasks undertaken
----------------

1.  Set up an empty git repository using bitbucket

2.  Find a gitignore for Unreal Engine 4 Projects from
    [https://github.com/github/gitignore/blob/master/UnrealEngine.gitignore](https://github.com/github/gitignore/blob/master/UnrealEngine.gitignore%20)
    and commit it using Sourcetree

3.  Set up the UE4 project as detailed in the 'Goals' section and commit
    it

What we found out
-----------------

In this Spike we learned that gitignores for popular project structures
and game engines are easily found online, as well as how to put them to
use. By creating a repository on Bitbucket we are now ready to continue
with the remaining Spikes, as well as being able to setup our own projects.
We also learned how to setup a basic UE4 project and got a glimpse at all
the different genres UE4 offers as base levels.

An important note is that a good practice is for the initial commit to contain
solely the gitignore file and perhaps also the README, if you push all contents
of a project alongside the gitignore the gitignore won't take effect and you 
will be left pushing data you don't need to.

Open Issues/risks
------------------

The process of setting up a git repository and using it through a client
can be difficult to remember for newer users, as such it should be
documented for team members depending on which method the team decides
to use.
